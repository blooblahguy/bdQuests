## Interface: 80000
## Title: |cffA02C2Fbd|rQuests
## Version: @project-version@
## DefaultState: Enabled
## LoadOnDemand: 0
## Dependencies: bdCore

init.lua
config.lua
functions.lua
frames.lua
core.lua